const SingletonClass = (function SingletonClass() {//closure for preserving states.
  let noOfInstances = 3; 
  let _this;
  /**@constructor */
  function SingletonClass(arg) {
    if (!new.target) throw new TypeError("Function is expected to be executed in constructor mode.");
    if (noOfInstances === 0) throw new Error("Class can't create instance as no available instance.");
    noOfInstances--;
    this.arg = arg;
    _this = this;
  }

  //privateVar
  let isSingletonAvailable = true;

  //static method
  SingletonClass.getAvailabilityStatus = function () {
    return isSingletonAvailable;
  };
  //static prototype method
  SingletonClass.prototype.getArgVal = function () {
    return this.arg;
  };
  return SingletonClass;
})();

//test code
let i = 5;
while (i > 0) {
  console.log(new SingletonClass('temp'+i).getArgVal());
  i--;
}
